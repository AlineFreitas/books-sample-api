describe('BookSampleApi', () => {
    let request;
    beforeAll(() =>{
        const app = require('../src/infrastructure/server') // Link to your server file
        const supertest = require('supertest')
        request = supertest(app)
    });

    it('GET /books Returns a list containing two books', async (done) => {
        const response = await request.get('/books')

        expect(response.status).toBe(200)
        expect(response.body.length).toBe(2)
        done()
    });
})