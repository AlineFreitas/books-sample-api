const port = process.env.PORT || 3000;
const server = require('./infrastructure/server');

server.listen(port, () => {
    console.log(`Started app on port ${port}`);
})