const router = require('express').Router();

router.get('/', async (request, response) => {
    return response.json([
        {
            id: '1',
            title: 'Clean Code'
        },
        {
            id: '2',
            title: 'Clean Architecture'
        }
    ]);
});

module.exports= router;